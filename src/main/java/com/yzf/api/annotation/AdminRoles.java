package com.yzf.api.annotation;

import com.yzf.api.type.RoleType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author
 * @author: quhc
 * @date: 2019/9/29 13:33
 */

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface AdminRoles {
    RoleType[] value() default {RoleType.ALL};
}
