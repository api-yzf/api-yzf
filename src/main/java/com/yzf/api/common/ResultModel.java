package com.yzf.api.common;

import lombok.Data;

@Data
public class ResultModel {
    /**
     * 返回码
     */
    private int code;

    /**
     * 返回结果描述
     */
    private String message;

    /**
     * 返回数据
     */
    private Object data;

    public ResultModel(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultModel(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResultModel(ResultStatus resultStatus) {
        this.code = resultStatus.getCode();
        this.message = resultStatus.getMsg();
    }

    public ResultModel(ResultStatus resultStatus, Object data) {
        this.code = resultStatus.getCode();
        this.message = resultStatus.getMsg();
        this.data = data;
    }

    public static ResultModel ok(Object data) {
        return new ResultModel(ResultStatus.SUCCESS, data);
    }

    public static ResultModel ok() {
        return new ResultModel(ResultStatus.SUCCESS);
    }

    public static ResultModel error(ResultStatus resultStatus) {
        return new ResultModel(resultStatus);
    }

    public static ResultModel error(int code, String message) {
        return new ResultModel(code, message);
    }

}
