package com.yzf.api.common;

public enum ResultStatus {
    SUCCESS(0, "成功"),
    INTERNAL_SERVER_ERROR(10001, "系统错误，请联系管理员"),
    FORBIDDEN(10002, "无操作权限"),
    PARAMETER_ERROR(10003, "参数错误"),
    DATA_NOT_FOUND(10004, "未找到数据"),
    DATA_EXIST(10005, "数据已存在"),
    TOKEN_INVALID(10007, "登陆失效，请重新登陆"),
    USER_NOT_EXIST(10008, "无此账号"),
    USER_FORBIDDEN(10009, "账号已暂停"),
    USERNAME_EXIST(10010, "用户名已经存在"),
    ROLE_NOT_EXIST(10011, "所选角色不存在"),
    MOBILE_EXIST(10012, "手机号已经注册"),
    
    GOODS_NAME_EXIST(20001, "该名称的商品已经存在"),
    GOODS_STOREROOM_NO_EXIST(20002, "库房不存在或不匹配"),
    GOODS_NOT_EXIST(20003, "商品不存在"),
    GOODS_STOCK_NOT_ENOUGH(20004, "商品库存不足"),
    GOODS_STOCK_STOP(2005, "商品暂停出库"),
    
    ORDER_NOT_EXIST(30001, "订单不存在"),
    ORDER_STATUS_ERROR(30002, "订单状态不符"),
    
    ORGAN_NAME_EXIST(40001, "店铺名已经存在"),
    
    ROOM_NAME_EXIST(50001, "库房名已经存在");

    private final int code;

    private final String msg;

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    ResultStatus(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public static ResultStatus getInstance(int code) {
        for(ResultStatus resultStatus : ResultStatus.values()) {
            if(resultStatus.getCode() == code) {
                return resultStatus;
            }
        }
        return null;
    }
}
