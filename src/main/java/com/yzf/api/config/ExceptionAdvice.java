package com.yzf.api.config;

import com.yzf.api.common.ResultModel;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.exeption.BusinessException;
import com.yzf.api.utils.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@ControllerAdvice
@ResponseBody
@Slf4j
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public ResultModel defaultErrorHandler(Exception ex) {
        log.error(ExceptionUtil.getMessage(ex));
        return new ResultModel(ResultStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    //运行时异常
    @ExceptionHandler(RuntimeException.class)
    public ResultModel runtimeExceptionHandler(RuntimeException ex) {
        log.error(ExceptionUtil.getMessage(ex));
        return new ResultModel(ResultStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    //系统自定义异常
    @ExceptionHandler(BusinessException.class)
    public ResultModel businessExceptionHandler(BusinessException ex) {
        Object data = ex.getData();
        if (data == null) {
            return new ResultModel(ex.getResultStatus());
        } else {
            return new ResultModel(ex.getResultStatus(), ex.getData());
        }
    }

    //参数校验异常
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResultModel methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        StringBuilder sb = new StringBuilder(100);
        allErrors.forEach(objectError -> {
            FieldError fieldError = (FieldError) objectError;
            sb.append(fieldError.getDefaultMessage()).append(";");
        });
        return ResultModel.error(ResultStatus.PARAMETER_ERROR.getCode(), sb.toString());
    }

    //400错误
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResultModel requestNotReadable(HttpMessageNotReadableException ex) {
        return new ResultModel(ResultStatus.PARAMETER_ERROR);
    }

    //400错误
    @ExceptionHandler({TypeMismatchException.class})
    public ResultModel requestTypeMismatch(TypeMismatchException ex) {
        return new ResultModel(ResultStatus.PARAMETER_ERROR);
    }

    //400错误
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResultModel requestMissingServletRequest(MissingServletRequestParameterException ex) {
        return new ResultModel(ResultStatus.PARAMETER_ERROR);
    }

    //405
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public ResultModel request405(HttpRequestMethodNotSupportedException ex) {
        return new ResultModel(ResultStatus.PARAMETER_ERROR);
    }
}
