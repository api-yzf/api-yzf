package com.yzf.api.controller;

import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.req.body.AdminAddBody;
import com.yzf.api.req.body.AdminLoginBody;
import com.yzf.api.service.AdminService;
import com.yzf.api.type.RoleType;
import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.message.ReusableMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = {"/api/admin"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AdminController extends BaseController{
    
    @Autowired
    private AdminService adminService;
    
    @RequestMapping(value="login", method= RequestMethod.POST)
    public ResultModel login(@Valid @RequestBody AdminLoginBody loginBody){
        return adminService.login(loginBody);
    }

    @RequestMapping(value="logout", method= RequestMethod.GET)
    @AdminRoles
    public ResultModel logout(){
        return adminService.logout(getAdmin());
    }
    
    @RequestMapping(value = "role/list", method = RequestMethod.GET)
    @AdminRoles
    public ResultModel roleList(){
        return adminService.getRoles();   
    }
    
    @RequestMapping(value="list", method = RequestMethod.GET)
    @AdminRoles
    public ResultModel list(@RequestParam(defaultValue = "0") Long roleId,
                            @RequestParam(defaultValue = "1") int pageNo,
                            @RequestParam(defaultValue = "10") int pageSize){
        return adminService.getByRole(roleId, pageNo, pageSize);
    }
    
    @RequestMapping(value="list/unbind", method = RequestMethod.GET)
    @AdminRoles({RoleType.ADMIN})
    public ResultModel listUnBind(@RequestParam(defaultValue = "0") Long roleId){
        return adminService.getUnbindAdminByRoleId(roleId);
    }

    @RequestMapping(value="status/update", method = RequestMethod.GET)
    @AdminRoles(value={RoleType.ADMIN})
    public ResultModel updateStatus(@RequestParam Long id, @RequestParam int status){
        return adminService.updateStatus(id, status);
    }

    @RequestMapping(value="info", method = RequestMethod.GET)
    @AdminRoles(value={RoleType.ADMIN})
    public ResultModel info(Long id){
        return adminService.getInfo(id);
    }

    @RequestMapping(value="update", method = RequestMethod.POST)
    @AdminRoles(value={RoleType.ADMIN})
    public ResultModel update(@RequestBody AdminAddBody body){
        return adminService.update(getAdmin(), body);
    }
    
    
    @RequestMapping(value="add", method = RequestMethod.POST)
    @AdminRoles(value={RoleType.ADMIN})
    public ResultModel add(@RequestBody AdminAddBody body){
        return adminService.add(getAdmin(), body);
    }
}
