package com.yzf.api.controller;

import com.yzf.api.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    
    @Autowired
    protected HttpServletRequest request;
    
    protected Admin getAdmin(){
        return (Admin) this.request.getAttribute("admin");
    }
    
    protected boolean isAdmin(){
        return (Boolean) this.request.getAttribute("isAdmin");
    }
}
