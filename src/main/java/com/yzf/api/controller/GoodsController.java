package com.yzf.api.controller;


import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.model.GoodsStockHistory;
import com.yzf.api.req.body.GoodsAddBody;
import com.yzf.api.req.body.GoodsStockBody;
import com.yzf.api.service.GoodsService;
import com.yzf.api.service.GoodsStockHistoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = {"/api/goods"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class GoodsController extends BaseController{
    
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsStockHistoryService goodsStockHistoryService;

    @AdminRoles
    @ApiOperation(value = "商品列表", notes = "")
    @RequestMapping(value="list", method= RequestMethod.GET)
    public ResultModel list(@RequestParam Long roomId,
                            @RequestParam(defaultValue = "") String name,
                            @RequestParam(defaultValue = "1") int pageNo,
                            @RequestParam(defaultValue = "10") int pageSize){
        return goodsService.listGoods(isAdmin(), roomId, name, pageNo, pageSize);
    }

    @AdminRoles
    @ApiOperation(value = "全部商品列表", notes = "")
    @RequestMapping(value="all", method = RequestMethod.GET)
    public ResultModel listAll(@RequestParam Long roomId){
        return goodsService.listAll(isAdmin(), roomId);
    }

    @AdminRoles
    @ApiOperation(value="商品详情", notes="")
    @RequestMapping(value="detail", method = RequestMethod.GET)
    public ResultModel detail(@RequestParam Long id, @RequestParam(defaultValue = "") String orderNo){
        return goodsService.detail(id, orderNo);
    }

    @AdminRoles
    @ApiOperation(value = "添加商品", notes = "")
    @RequestMapping(value="add", method = RequestMethod.POST)
    public ResultModel add(@RequestBody GoodsAddBody goodsAddBody){
        return goodsService.addGoods(goodsAddBody, getAdmin());
    }

    @AdminRoles
    @ApiOperation(value = "修改商品", notes = "")
    @RequestMapping(value="update", method = RequestMethod.POST)
    public ResultModel update(@RequestBody GoodsAddBody goodsAddBody){
        return goodsService.updateGoods(isAdmin(), goodsAddBody);
    }

    @AdminRoles
    @ApiOperation(value = "添加修改库存", notes = "")
    @RequestMapping(value="stock/update", method = RequestMethod.POST)
    public ResultModel addStock(@RequestBody GoodsStockBody goodsStockBody){
        return goodsService.updateStock(goodsStockBody, getAdmin());
    }

    @AdminRoles
    @ApiOperation(value = "添加出库状态", notes = "")
    @RequestMapping(value="status/update", method = RequestMethod.POST)
    public ResultModel updateStatus(@RequestBody GoodsAddBody goodsAddBody){
        return goodsService.updateStockStatus(goodsAddBody);
    }

    @AdminRoles
    @ApiOperation(value = "商品库存流水列表", notes = "")
    @RequestMapping(value="stockhistory/list", method = RequestMethod.GET)
    public ResultModel listHistory(@RequestParam(defaultValue = "0") Long goodsId,
                                   @RequestParam String startDate,
                                   @RequestParam String endDate,
                                   @RequestParam(defaultValue = "0") Integer type,
                                   @RequestParam(defaultValue = "0") Long storeroomId,
                                   @RequestParam(defaultValue = "0") Long organId,
                                   @RequestParam(defaultValue = "1") int pageNo,
                                   @RequestParam(defaultValue = "10") int pageSize){
        return goodsStockHistoryService.list(goodsId, organId, startDate, endDate, type, storeroomId, pageNo, pageSize);
    }
}
