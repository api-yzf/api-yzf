package com.yzf.api.controller;

import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.req.body.OrderAddBody;
import com.yzf.api.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = {"/api/order"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OrderController extends BaseController{
    
    @Autowired
    private OrderService orderService;

    @AdminRoles
    @ApiOperation(value = "订单列表", notes = "")
    @RequestMapping(value="list", method= RequestMethod.GET)
    public ResultModel list(@RequestParam(defaultValue = "") String orderNo,
                            @RequestParam(defaultValue = "0") Integer orderState,
                            @RequestParam(defaultValue = "0") Long organId,
                            @RequestParam(defaultValue = "1") int pageNo,
                            @RequestParam(defaultValue = "10") int pageSize){
        return orderService.list(orderNo, orderState, organId, pageNo, pageSize); 
    }

    @AdminRoles
    @ApiOperation(value = "订单详情", notes = "")
    @RequestMapping(value="detail", method= RequestMethod.GET)
    public ResultModel detail(@RequestParam String orderNo){
        return orderService.getDetail(orderNo);
    }

    @AdminRoles
    @ApiOperation(value = "确认出货", notes = "")
    @RequestMapping(value="out/confirm", method= RequestMethod.GET)
    public ResultModel confirmOut(@RequestParam String orderNo){
        return orderService.confirmOut(orderNo, getAdmin());
    }

    @AdminRoles
    @ApiOperation(value = "取消出库", notes = "")
    @RequestMapping(value="out/cancel", method= RequestMethod.GET)
    public ResultModel cancelOut(@RequestParam String orderNo){
        return orderService.cancel(orderNo, getAdmin());
    }

    @AdminRoles
    @ApiOperation(value = "确认到货", notes = "")
    @RequestMapping(value="in/confirm", method= RequestMethod.GET)
    public ResultModel confirmIn(@RequestParam String orderNo){
        return orderService.confirmIn(orderNo);
    }

    @AdminRoles
    @ApiOperation(value = "进货（添加订单）", notes = "")
    @RequestMapping(value="add", method= RequestMethod.POST)
    public ResultModel add(@RequestBody OrderAddBody orderAddBody){
        return orderService.add(orderAddBody, getAdmin());
    }
    
    @AdminRoles
    @ApiOperation(value = "修改订单", notes = "")
    @RequestMapping(value="update", method= RequestMethod.POST)
    public ResultModel update(@RequestBody OrderAddBody orderAddBody){
        return orderService.update(orderAddBody, getAdmin());
    }
}
