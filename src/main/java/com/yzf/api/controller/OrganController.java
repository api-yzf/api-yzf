package com.yzf.api.controller;

import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.req.body.OrganAddBody;
import com.yzf.api.req.body.OrganAdminsUpdateBody;
import com.yzf.api.service.OrganService;
import com.yzf.api.type.RoleType;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/api/organ"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OrganController extends BaseController{
    
    @Autowired
    private OrganService organService;

    @AdminRoles
    @ApiOperation(value = "全部门店", notes = "")
    @RequestMapping(value="all", method= RequestMethod.GET)
    public ResultModel listAll(){
        return organService.listAll();
    }
    
    
    @RequestMapping(value="add", method = RequestMethod.POST)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel add(@RequestBody OrganAddBody body){
        return organService.add(getAdmin(), body);
    }

    @RequestMapping(value="update", method = RequestMethod.POST)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel update(@RequestBody OrganAddBody body){
        return organService.update(getAdmin(), body);
    }
    
    @RequestMapping(value="status/update", method=RequestMethod.GET)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel updateStatus(Long id, int status){
        return organService.updateStatus(id, status);
    }

    @RequestMapping(value="admins", method=RequestMethod.GET)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel admins(Long id){
        return organService.getAdmins(id);
    }
    
    @RequestMapping(value="admins/update", method=RequestMethod.POST)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel updateAdmin(@RequestBody OrganAdminsUpdateBody body){
        return organService.updateAdmins(body);
    }
}
