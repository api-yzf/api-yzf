package com.yzf.api.controller;

import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.service.StateService;
import com.yzf.api.type.RoleType;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/api/stat"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class StatController {
    
    @Autowired
    StateService stateService;

    @AdminRoles(value = {RoleType.ADMIN, RoleType.BASE_ORGAN_MANAGER})
    @ApiOperation(value = "店面订单统计", notes = "")
    @RequestMapping(value="organ/order", method = RequestMethod.GET)
    public ResultModel organOrder(@RequestParam(defaultValue = "") String startDate,
                                  @RequestParam(defaultValue = "") String endDate,
                                  @RequestParam(defaultValue = "0") Integer type,
                                  @RequestParam(defaultValue = "0") Long organId,
                                  @RequestParam(defaultValue = "1") int pageNo,
                                  @RequestParam(defaultValue = "10") int pageSize){
        
        return stateService.getOrganOrderList(organId, type, startDate, endDate, pageNo, pageSize);
    }

    @AdminRoles(value = {RoleType.ADMIN, RoleType.BASE_ORGAN_MANAGER})
    @ApiOperation(value = "仓库订单统计", notes = "")
    @RequestMapping(value="room/order", method = RequestMethod.GET)
    public ResultModel roomOrder(@RequestParam(defaultValue = "") String startDate,
                                 @RequestParam(defaultValue = "") String endDate,
                                 @RequestParam(defaultValue = "1") Integer type,
                                 @RequestParam(defaultValue = "0") Long roomId,
                                 @RequestParam(defaultValue = "1") int pageNo,
                                 @RequestParam(defaultValue = "10") int pageSize){
        return  stateService.getRoomOrderList(roomId, type, startDate, endDate, pageNo, pageSize);
    }

    @AdminRoles(value = {RoleType.ADMIN, RoleType.BASE_ORGAN_MANAGER})
    @ApiOperation(value = "订单统计", notes = "")
    @RequestMapping(value="order/stat", method = RequestMethod.GET)
    public ResultModel orderStat(@RequestParam(defaultValue = "") String startDate,
                                 @RequestParam(defaultValue = "") String endDate,
                                 @RequestParam(defaultValue = "0") Long organId,
                                 @RequestParam(defaultValue = "0") Long roomId){
        return stateService.getOrderStat(organId, roomId, startDate, endDate);
    }
}
