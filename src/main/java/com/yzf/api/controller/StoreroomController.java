package com.yzf.api.controller;

import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.req.body.StoreroomAddBody;
import com.yzf.api.req.body.StoreroomAdminsUpdateBody;
import com.yzf.api.service.StoreroomService;
import com.yzf.api.type.RoleType;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/api/storeroom"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class StoreroomController extends BaseController{
    
    @Autowired
    private StoreroomService storeroomService;

    @AdminRoles
    @ApiOperation(value = "全部库房", notes = "")
    @RequestMapping(value="all", method= RequestMethod.GET)
    public ResultModel listAll(){
        return storeroomService.listAll();
    }
    
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel add(@RequestBody StoreroomAddBody body){
        return storeroomService.add(getAdmin(), body);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel update(@RequestBody StoreroomAddBody body){
        return storeroomService.update(getAdmin(), body);
    }

    @RequestMapping(value = "status/update", method = RequestMethod.GET)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel updateStatus(Long id, int status){
        return storeroomService.updateStatus(id, status);
    }

    @RequestMapping(value = "admins", method = RequestMethod.GET)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel admins(Long id){
        return storeroomService.getAdmins(id);
    }

    @RequestMapping(value = "admins/update", method = RequestMethod.POST)
    @AdminRoles(value = {RoleType.ADMIN})
    public ResultModel updateAdmins(@RequestBody StoreroomAdminsUpdateBody body){
        return storeroomService.updateAdmins(body);
    }
}
