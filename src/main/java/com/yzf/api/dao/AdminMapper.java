package com.yzf.api.dao;

import com.yzf.api.model.Admin;
import com.yzf.api.pojo.AdminDetail;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface AdminMapper extends Mapper<Admin> {
    
    Admin getAdminByMobile(@Param("mobile") String mobile);
    
    Admin getByName(@Param("name") String name);
    
    List<AdminDetail> getByRole(@Param("roleId") Long roleId);
    
    List<Admin> getOrganAdminsByRoleId(@Param("organId") Long organId, @Param("roleId") Long roleId);
    
    List<Admin> getStoreroomAdminsByRoleId(@Param("roomId") Long roomId, @Param("roleId") Long roleId);
    
    List<Admin> getUnbindOrganAdminsByRoleId(Long roleId);
    
    List<Admin> getUnbindStoreroomAdminsByRoleId(Long roleId);
}
