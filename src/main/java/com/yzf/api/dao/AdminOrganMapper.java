package com.yzf.api.dao;


import com.yzf.api.model.AdminOrgan;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AdminOrganMapper extends BaseMapper<AdminOrgan> {
    
    List<AdminOrgan> getByRoleId(@Param("organId") Long organId, @Param("roleId") Long roleId);
    
    void deleteByOrganId(Long organId);
}
