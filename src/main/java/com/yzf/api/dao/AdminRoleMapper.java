package com.yzf.api.dao;

import com.yzf.api.model.AdminRole;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AdminRoleMapper extends BaseMapper<AdminRole> {
    
    List<AdminRole> getListByAdminId(@Param("adminId") Long adminId);
}
