package com.yzf.api.dao;

import com.yzf.api.model.AdminStoreroom;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AdminStoreroomMapper extends BaseMapper<AdminStoreroom> {
    
    List<AdminStoreroom> getByRoleId(@Param("roomId") Long roomId, @Param("roleId") Long roleId);
    
    void deleteByRoomId(Long roomId);
}
