package com.yzf.api.dao;

import com.yzf.api.model.Goods;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface GoodsMapper extends BaseMapper<Goods> {
    
    List<Goods> getGoodsListByStoreroomId(@Param("roomId") Long storeroomId, @Param("name") String name, @Param("isAdmin") boolean isAdmin);
    
    Goods getGoodsByName(@Param("name") String name, @Param("storeroomId") Long storeroomId);
    
//    void addStock(@Param("quantity") Integer quantity, @Param("id") Long id, @Param("storeroom_id") Long storeroom_id);
    
}
