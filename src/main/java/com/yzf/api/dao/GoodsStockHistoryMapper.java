package com.yzf.api.dao;

import com.yzf.api.model.GoodsStockHistory;
import com.yzf.api.pojo.GoodsStockHistoryDetail;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface GoodsStockHistoryMapper extends BaseMapper<GoodsStockHistory> {
    
    List<GoodsStockHistoryDetail> getList(@Param("goodsId") Long goodsId,
                                          @Param("organId") Long organId,
                                          @Param("startDate") String startDate,
                                          @Param("endDate") String endDate, @Param("type") Integer type,
                                          @Param("storeroomId") Long storeroomId);
}
