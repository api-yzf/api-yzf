package com.yzf.api.dao;

import com.yzf.api.model.OrderDetail;
import com.yzf.api.model.BaseOrder;
import com.yzf.api.pojo.OrderStat;
import com.yzf.api.pojo.OrderStatDetail;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.math.BigDecimal;
import java.util.List;

public interface OrderMapper extends BaseMapper<BaseOrder> {
    List<BaseOrder> getList(@Param("orderNo") String orderNo,
                            @Param("orderState") Integer orderState,
                            @Param("organId") Long organId);
    
    List<OrderDetail> getDetailList(@Param("orderId") Long orderId);
    
    BaseOrder getOrderByNo(@Param("orderNo") String orderNo);
    
    void insertDetailBatch(List<OrderDetail> list);
    
    void clearDetail(@Param("orderId") Long orderId);
    
    void updateUnConfirmedGoodsPrice(@Param("goodsPrice") BigDecimal goodsPrice, @Param("id") Long id);
    
    List<OrderDetail> getUnConfirmedDetailByGoodsId(@Param("goodsId") Long goodsId);
    
    void reCalcAmount(@Param("id") Long id);
    
    OrderStat getStatSum(@Param("organId") Long organId,
                              @Param("roomId") Long roomId,
                              @Param("startDate") String startDate,
                              @Param("endDate") String endDate);
    
    List<OrderStatDetail> getStatListByDay(@Param("organId") Long organId,
                                           @Param("roomId") Long roomId,
                                           @Param("startDate") String startDate,
                                           @Param("endDate") String endDate);
    
    List<OrderStatDetail> getStatListByMonth(@Param("organId") Long organId,
                                             @Param("roomId") Long roomId,
                                             @Param("startDate") String startDate,
                                             @Param("endDate") String endDate);
}
