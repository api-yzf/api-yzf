package com.yzf.api.dao;

import com.yzf.api.model.Organ;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface OrganMapper extends BaseMapper<Organ> {
    
    List<Organ> getOrganByAdminId(@Param("adminId") Long adminId);
    
    Organ getByName(String name);
}
