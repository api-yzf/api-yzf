package com.yzf.api.dao;

import com.yzf.api.model.OrganStoreroom;
import tk.mybatis.mapper.common.BaseMapper;

public interface OrganStoreroomMapper extends BaseMapper<OrganStoreroom> {
    
    OrganStoreroom getByOrganId(Long organId);
}
