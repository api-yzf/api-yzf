package com.yzf.api.dao;

import com.yzf.api.model.Role;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role> {
    
    List<Role> getRolesByAdminId(@Param("adminId") Long adminId);
}
