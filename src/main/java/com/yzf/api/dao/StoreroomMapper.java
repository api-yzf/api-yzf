package com.yzf.api.dao;

import com.yzf.api.model.Storeroom;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface StoreroomMapper extends BaseMapper<Storeroom> {
    
    List<Storeroom> getStoreroomByAdminId(@Param("adminId") Long adminId);
    
    Storeroom getByName(String name);
    
    Storeroom getStoreroomByOrganId(Long organId);
}
