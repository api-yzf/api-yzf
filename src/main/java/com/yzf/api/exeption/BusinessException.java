package com.yzf.api.exeption;

import com.yzf.api.common.ResultStatus;
import lombok.Data;

@Data
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private ResultStatus resultStatus;
    private Object data;

    public BusinessException(ResultStatus resultStatus) {
        super();
        this.resultStatus = resultStatus;
    }

    public BusinessException(ResultStatus resultStatus, Object data) {
        super();
        this.resultStatus = resultStatus;
        this.data = data;
    }
}
