package com.yzf.api.interceptor;

import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.dao.AdminMapper;
import com.yzf.api.dao.RoleMapper;
import com.yzf.api.exeption.BusinessException;
import com.yzf.api.model.Admin;
import com.yzf.api.model.Role;
import com.yzf.api.service.RedisService;
import com.yzf.api.type.RoleType;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.List;

public class AuthenticationInterceptor implements HandlerInterceptor {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Value("${jwt.secret.key}")
    private String jwtSecretKey;
    @Autowired
    private RedisService redisService;
    
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception{
        if (!(object instanceof HandlerMethod)) {
            return false;
        }
        if(false){
            Admin admin = adminMapper.selectByPrimaryKey(1L);
            if(admin == null){
                throw new BusinessException(ResultStatus.DATA_NOT_FOUND);
            }
            List<Role> roles = roleMapper.getRolesByAdminId(1L);
            admin.setRoles(roles);

            httpServletRequest.setAttribute("admin", admin);
            return true;
        }
        
        HandlerMethod handlerMethod = (HandlerMethod) object;
        Method method = handlerMethod.getMethod();
        if (method.isAnnotationPresent(AdminRoles.class)) {
            AdminRoles adminRoles = method.getAnnotation(AdminRoles.class);
            final String token = httpServletRequest.getHeader("Authorization");
            if (StringUtils.isBlank(token)) {
                throw new BusinessException(ResultStatus.TOKEN_INVALID);
            }
            if (StringUtils.isBlank(redisService.get(token))) {
                throw new BusinessException(ResultStatus.TOKEN_INVALID);
            }
            long adminId;
            try {
                final Claims claims = Jwts.parser().setSigningKey(this.jwtSecretKey).parseClaimsJws(token).getBody();
                adminId = Long.parseLong(claims.getSubject());
            } catch (Exception e) {
                throw new BusinessException(ResultStatus.TOKEN_INVALID);
            }
            Admin admin = adminMapper.selectByPrimaryKey(adminId);
            if (admin == null || admin.getStatus() != 1) {
                throw new BusinessException(ResultStatus.DATA_NOT_FOUND);
            }
            
            admin.setToken(token);
            List<Role> roles = roleMapper.getRolesByAdminId(adminId);
            admin.setRoles(roles);
            
            if(!checkRole(roles, adminRoles)){
                throw new BusinessException(ResultStatus.FORBIDDEN);
            }
            
            boolean isAdmin = false;
            for(Role role: roles){
                if(role.getName().equals("admin")){
                    isAdmin = true;
                }
            }
            httpServletRequest.setAttribute("isAdmin", isAdmin);

            httpServletRequest.setAttribute("admin", admin);
        }
        return true;
    }
    
    private boolean checkRole(List<Role> roles, AdminRoles adminRoles){
        if(roles == null || roles.size() == 0){
            return false;
        }
        
        Role role = roles.get(0);
        RoleType[] roleTypes = adminRoles.value();
        for(int i=0; i<roleTypes.length;i++){
            RoleType roleType = roleTypes[i];
            if(roleType == RoleType.ALL){
                return true;
            }else if(roleType.getDesc().toLowerCase().equals(role.getName().toLowerCase())){
                return true;
            }
        }
        return false;
    }
}
