package com.yzf.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties({"pwd","salt"})
public class Admin {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    private Long id;
    private String mobile;
    private String name;
    private String realname;
    private String pwd;
    private String salt;
    private Integer status;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    private Long createBy;
    @Transient
    private String token;
    @Transient
    private List<Role> roles;
    @Transient
    private List<Organ> organs;
    @Transient
    private List<Storeroom> storerooms;
}
