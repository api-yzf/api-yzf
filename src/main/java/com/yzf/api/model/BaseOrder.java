package com.yzf.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class BaseOrder {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    @Column
    private Long id;
    @Column
    private String orderNo;
    @Column
    private Long storeroomId;
    @Column
    private Long organId;
    @Column
    private BigDecimal amount;
    @Column
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeDate;
    @Column
    private Date createdDate;
    @Column
    private Integer orderStatus;
    @Column
    private Integer status;
    @Transient
    private List<OrderDetail> goodsList;
    @Column
    private Long adminId;
    @Column
    private Integer delivery;
    @Column
    private String originInfo;
}
