package com.yzf.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class Goods {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    @Column
    private Long id;
    @Column
    private Long storeroomId;
    @Column
    private String name;
    @Column
    private String unit;
    @Column
    private BigDecimal price;
    @Column
    private Integer stock;
    @Column
    private Integer frozenStock;
    @Column
    private Integer status;
    @Column
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    @Column
    private Integer stockStatus;
    @Column
    private Long adminId;
}
