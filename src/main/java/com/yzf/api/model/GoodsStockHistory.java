package com.yzf.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

@Data
public class GoodsStockHistory {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    @Column
    private Long id;
    @Column
    private Long goodsId;
    @Column
    private String goodsName;
    @Column
    private Long roomId;
    @Column
    private String roomName;
    @Column
    private Long organId;
    @Column
    private String organName;
    @Column
    private Integer stock;
    @Column
    private Integer remaindingStock;
    @Column
    private Integer frozenStock;
    @Column
    private Integer remaindingFrozenStock;
    @Column
    private String remark;
    @Column
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    @Column
    private Long orderId;
    @Column
    private Long adminId;
    @Column
    private Integer type;
}
