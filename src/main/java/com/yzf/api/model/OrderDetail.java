package com.yzf.api.model;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Id;
import java.math.BigDecimal;

@Data
public class OrderDetail {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    private Long id;
    private Long orderId;
    private Long goodsId;
    private String goodsName;
    private Integer quantity;
    private BigDecimal orderPrice;
    private BigDecimal goodsPrice;
    private String unit;
    private Integer remain;
}
