package com.yzf.api.model;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Data
public class Organ {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    private Long id;
    @Column
    private String name;
    @Column
    
    private Date createdDate;
    @Column
    private Long createBy;
    @Column
    private Integer status;
    @Transient
    private Admin admin;
    @Transient
    private List<Admin> staffs;
    @Transient
    private Storeroom storeroom;
}
