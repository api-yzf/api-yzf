package com.yzf.api.model;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Id;

@Data
public class Role {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    private Long id;
    private String name;
    private String title;
}
