package com.yzf.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Data
public class Storeroom {
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    @Column
    private Long id;
    @Column
    private String name;
    @Column
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    @Column
    private Long createBy;
    @Column
    private Integer status;
    @Transient
    private List<Admin> staffs;
    @Transient
    private Admin admin;
}
