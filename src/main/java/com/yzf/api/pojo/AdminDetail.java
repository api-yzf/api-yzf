package com.yzf.api.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class AdminDetail {
    private Long id;
    private String mobile;
    private String name;
    private String realname;
    private String pwd;
    private Integer status;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    private Long createBy;
    private Long organId;
    private Long roomId;
    private String organName;
    private String roomName;
    private Long roleId;
    private String roleName;
}
