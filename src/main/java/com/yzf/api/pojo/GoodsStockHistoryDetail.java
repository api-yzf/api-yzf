package com.yzf.api.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class GoodsStockHistoryDetail {
    private Long id;
    private Long goodsId;
    private String goodsName;
    private Long roomId;
    private String roomName;
    private Long organId;
    private String organName;
    private String unit;
    private Integer stock;
    private String remark;
    private Date createdDate;
    private Long orderId;
    private Integer type;
}
