package com.yzf.api.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderStat {
    private BigDecimal amount;
    private Long quantity;
//    private List<OrderStatDetail> list;
}
