package com.yzf.api.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderStatDetail {
//    private Long organId;
//    private Long roomId;
    private String dt;
    private BigDecimal orderAmount;
    private Long quantity;
}
