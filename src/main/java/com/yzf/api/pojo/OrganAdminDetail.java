package com.yzf.api.pojo;

import com.yzf.api.model.Admin;
import lombok.Data;

import java.util.List;

@Data
public class OrganAdminDetail {
    Admin keeper;
    List<Admin> staffs;
}
