package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class AdminAddBody {
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String pwd;
    @NotEmpty
    private String realname;
    @NotEmpty(message = "手机号不能为空")
    @Pattern(regexp = "1[0-9]\\d{9}", message = "手机号格式错误")
    private String mobile;
    @NotNull
    private Long roleId;
    private Integer status=1;
}
