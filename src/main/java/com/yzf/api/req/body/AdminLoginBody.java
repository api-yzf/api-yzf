package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AdminLoginBody {
    
    @NotBlank(message = "手机号不能为空")
    private String mobile;
    
    @NotBlank(message = "密码不能为空")
    private String pwd;
}
