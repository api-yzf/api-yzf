package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class GoodsAddBody {
    private Long id=0L;
    @NotBlank
    private Long storeroomId;
    @NotBlank(message = "商品名称不能为空")
    private String name;
    @NotBlank(message = "商品单位不能为空")
    private String unit;
    @DecimalMin(value = "0", message = "价格错误")
    @DecimalMax(value = "100000", message = "价格错误")
    private BigDecimal price;
    private Integer status;
    private Integer stockStatus;
}
