package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class GoodsStockBody {
    @NotBlank
    private Long id;
    @NotBlank
    private Integer stock;
    @Min(value = 1)
    @Max(value = 2)
    private Integer type;
    private String remark;
    private Long orderId;
}
