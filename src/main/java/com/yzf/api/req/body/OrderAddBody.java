package com.yzf.api.req.body;

import com.yzf.api.model.Goods;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class OrderAddBody {
    private String orderNo;
    @NotBlank
    private Long organId;
    @NotBlank
    private Long storeroomId;
    @NotBlank
    private List<GoodsStockBody> goodsList;
    /**
     * 配送方式 1:自动 2:普通快送
     */
    @Min(1)
    @Max(2)
    private Integer delivery;
}
