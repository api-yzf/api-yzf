package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class OrganAddBody {
    private Long id;
    @NotBlank
    private String name;
    @NotNull
    private Long adminId;
    @NotNull
    private Long roomId;
    private Integer status = 0;
}
