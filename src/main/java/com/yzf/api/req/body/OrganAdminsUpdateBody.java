package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OrganAdminsUpdateBody {
    @NotNull
    private Long organId;
    private Long keeperId;
    private List<Long> staffs;
}
