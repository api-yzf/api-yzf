package com.yzf.api.req.body;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class StoreroomAddBody {
    private Long id;
    @NotEmpty
    private String name;
    @NotNull
    private Long adminId;
    private Integer status=1;
}
