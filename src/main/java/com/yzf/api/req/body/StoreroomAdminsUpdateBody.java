package com.yzf.api.req.body;

import lombok.Data;

import java.util.List;

@Data
public class StoreroomAdminsUpdateBody {
    private Long roomId;
    private Long keeperId;
    private List<Long> staffs;
}
