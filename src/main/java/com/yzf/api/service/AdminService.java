package com.yzf.api.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yzf.api.annotation.AdminRoles;
import com.yzf.api.common.ResultModel;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.dao.*;
import com.yzf.api.model.*;
import com.yzf.api.pojo.AdminDetail;
import com.yzf.api.req.body.AdminAddBody;
import com.yzf.api.req.body.AdminLoginBody;
import com.yzf.api.security.JwtTokenProvider;
import com.yzf.api.type.AdminStatusType;
import com.yzf.api.utils.MD5Util;
import com.yzf.api.utils.RoleUtil;
import com.yzf.api.utils.TextUtil;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private OrganMapper organMapper;
    @Autowired
    private StoreroomMapper storeroomMapper;
    @Autowired
    private RedisService redisService;
    @Autowired
    private AdminOrganMapper adminOrganMapper;
    @Autowired
    private AdminStoreroomMapper adminStoreroomMapper;
    @Autowired
    private AdminRoleMapper adminRoleMapper;
    
    public ResultModel login(AdminLoginBody loginBody){
        Admin admin = adminMapper.getAdminByMobile(loginBody.getMobile());
        if(admin == null){
            return ResultModel.error(ResultStatus.USER_NOT_EXIST);
        }
        if(admin.getStatus() != AdminStatusType.NORMAL.getType()){
            return ResultModel.error(ResultStatus.USER_FORBIDDEN);
        }
        String password = MD5Util.MD5Encode(String.format("%s%s", loginBody.getPwd(), admin.getSalt()));
        if(!password.equalsIgnoreCase(admin.getPwd())){
            return ResultModel.error(ResultStatus.USER_NOT_EXIST);
        }
        String token = jwtTokenProvider.createToken(admin.getId());
        admin.setToken(token);
        admin.setRoles(roleMapper.getRolesByAdminId(admin.getId()));
        admin.setOrgans(organMapper.getOrganByAdminId(admin.getId()));
        admin.setStorerooms(storeroomMapper.getStoreroomByAdminId(admin.getId()));
        redisService.set(token, admin.getId().toString(), 30);
        
        return ResultModel.ok(admin);
    }
    
    public ResultModel logout(Admin admin){
         redisService.del(admin.getToken());
         return  ResultModel.ok();
    }
    
    public ResultModel getRoles(){
        List<Role> roles = roleMapper.selectAll();
        return ResultModel.ok(roles);
    }
    
    public ResultModel getByRole(Long roleId, int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<AdminDetail> admins = adminMapper.getByRole(roleId);
        PageInfo<AdminDetail> pageInfo = new PageInfo<>(admins);
        return ResultModel.ok(pageInfo);
    }
    
    @Synchronized
    public ResultModel add(Admin admin, AdminAddBody body){
        Admin newAdmin = new Admin();
        if(adminMapper.getByName(body.getName()) != null){
            return ResultModel.error(ResultStatus.USERNAME_EXIST);
        }
        
        if(adminMapper.getAdminByMobile(body.getMobile()) != null){
            return ResultModel.error(ResultStatus.MOBILE_EXIST);
        }
        
        Role role = roleMapper.selectByPrimaryKey(body.getRoleId());
        if(role == null){
            return ResultModel.error(ResultStatus.ROLE_NOT_EXIST);
        }
        
        newAdmin.setName(body.getName());
        newAdmin.setRealname(body.getRealname());
        newAdmin.setMobile(body.getMobile());
        String salt = TextUtil.genRndStr(6, false);
        newAdmin.setSalt(salt);
        String enPwd = MD5Util.MD5Encode(String.format("%s%s", body.getPwd(), salt));
        newAdmin.setPwd(enPwd);
        newAdmin.setCreateBy(admin.getId());
        
        adminMapper.insertSelective(newAdmin);
        
        AdminRole adminRole = new AdminRole();
        adminRole.setAdminId(newAdmin.getId());
        adminRole.setRoleId(role.getId());
        adminRoleMapper.insert(adminRole);
        
        return ResultModel.ok();
    }
    
    @Synchronized
    public ResultModel update(Admin admin, AdminAddBody body){
        Long id = body.getId();
        if(id == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        Admin adm = adminMapper.selectByPrimaryKey(id);
        if(adm == null){
            return ResultModel.error(ResultStatus.USER_NOT_EXIST);
        }

        if(!adm.getName().equals(body.getName())) {
            if (adminMapper.getByName(body.getName()) != null) {
                return ResultModel.error(ResultStatus.USERNAME_EXIST);
            }
        }

        if(!adm.getMobile().equals(body.getMobile())) {
            if (adminMapper.getAdminByMobile(body.getMobile()) != null) {
                return ResultModel.error(ResultStatus.MOBILE_EXIST);
            }
        }

        Role role = roleMapper.selectByPrimaryKey(body.getRoleId());
        if(role == null){
            return ResultModel.error(ResultStatus.ROLE_NOT_EXIST);
        }
        
        adm.setName(body.getName());
        adm.setRealname(body.getRealname());
        adm.setMobile(body.getMobile());
        String enPwd = MD5Util.MD5Encode(String.format("%s%s", body.getPwd(), adm.getSalt()));
        adm.setPwd(enPwd);
        
        adminMapper.updateByPrimaryKeySelective(adm);
        
        List<AdminRole> adminRoleList = adminRoleMapper.getListByAdminId(adm.getId());
        if(adminRoleList != null && adminRoleList.size() > 0){
            for(AdminRole ar: adminRoleList){
                adminRoleMapper.deleteByPrimaryKey(ar.getId());
            }
        }
        AdminRole adminRole = new AdminRole();
        adminRole.setAdminId(adm.getId());
        adminRole.setRoleId(role.getId());
        adminRoleMapper.insert(adminRole);
        
        return ResultModel.ok();
    }
    
    public ResultModel getInfo(Long id){
        Admin admin = adminMapper.selectByPrimaryKey(id);
        if(admin == null){
            return ResultModel.error(ResultStatus.USER_NOT_EXIST);
        }
        
        return ResultModel.ok(admin);
    }
    
    public ResultModel updateStatus(Long id, int status){
        Admin admin = adminMapper.selectByPrimaryKey(id);
        if(admin == null){
            return ResultModel.error(ResultStatus.USER_NOT_EXIST);
        }
        
        admin.setStatus(status);
        adminMapper.updateByPrimaryKeySelective(admin);
        
        return ResultModel.ok();
    }
    
    public void updateRoleRelation(Long organId, Long roomId, Long adminId, Role role){
        if(RoleUtil.isOrganMember(role)){
            List<AdminOrgan> adminOrganList = adminOrganMapper.getByRoleId(organId, role.getId());
            if(adminOrganList!=null && adminOrganList.size()>0){
                for (AdminOrgan adminOrgan : adminOrganList) {
                    adminOrganMapper.deleteByPrimaryKey(adminOrgan.getId());
                }
            }
            AdminOrgan adminOrgan = new AdminOrgan();
            adminOrgan.setOrganId(organId);
            adminOrgan.setAdminId(adminId);
            adminOrganMapper.insert(adminOrgan);
        }else if(RoleUtil.isStoreroomMember(role)){
            List<AdminStoreroom> adminStoreroomList = adminStoreroomMapper.getByRoleId(roomId, role.getId());
            if(adminStoreroomList!=null && adminStoreroomList.size()>0){
                for (AdminStoreroom adminStoreroom: adminStoreroomList){
                    adminStoreroomMapper.deleteByPrimaryKey(adminStoreroom.getId());
                }
            }
            AdminStoreroom adminStoreroom = new AdminStoreroom();
            adminStoreroom.setStoreroomId(roomId);
            adminStoreroom.setAdminId(adminId);
            adminStoreroomMapper.insert(adminStoreroom);
        }
    }
    
    public ResultModel getUnbindAdminByRoleId(Long roleId){
        if(roleId == null || roleId.intValue() == 0){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Role role = roleMapper.selectByPrimaryKey(roleId);
        if(role == null){
            return ResultModel.error(ResultStatus.ROLE_NOT_EXIST);
        }
        
        List<Admin> ls;
        if(RoleUtil.isOrganMember(role)){
            ls = adminMapper.getUnbindOrganAdminsByRoleId(role.getId());
        }else{
            ls = adminMapper.getUnbindStoreroomAdminsByRoleId(role.getId());
        }
        
        return ResultModel.ok(ls);
    }
}
