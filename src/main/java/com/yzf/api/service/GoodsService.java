package com.yzf.api.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yzf.api.common.ResultModel;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.dao.*;
import com.yzf.api.model.*;
import com.yzf.api.req.body.GoodsAddBody;
import com.yzf.api.req.body.GoodsStockBody;
import com.yzf.api.type.GoodsStockHistoryType;
import com.yzf.api.type.OrderStatusType;
import lombok.Synchronized;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GoodsService {
    
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private StoreroomMapper storeroomMapper;
    @Autowired
    private GoodsStockHistoryMapper goodsStockHistoryMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrganMapper organMapper;
    
    public ResultModel listGoods(boolean isAdmin, Long storeroomId, String name, int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<Goods> goodsList = goodsMapper.getGoodsListByStoreroomId(storeroomId, name, isAdmin);
        PageInfo<Goods> pageInfo = new PageInfo<>(goodsList);
        return ResultModel.ok(pageInfo);
    }
    
    public ResultModel listAll(boolean isAdmin, Long storeroomId){
        List<Goods> goodsList = goodsMapper.getGoodsListByStoreroomId(storeroomId, null, isAdmin);
        return ResultModel.ok(goodsList);
    }
    
    public ResultModel detail(Long id, String orderNo){
        Goods goods = goodsMapper.selectByPrimaryKey(id);
        if(StringUtils.isNotBlank(orderNo)) {
            BaseOrder baseOrder = orderMapper.getOrderByNo(orderNo);
            if(baseOrder != null && baseOrder.getOrderStatus() == OrderStatusType.UNCONFIRMED.getType()){
                List<OrderDetail> orderDetailList = orderMapper.getDetailList(baseOrder.getId());
                orderDetailList.forEach(item->{
                    if(item.getGoodsId().equals(goods.getId())){
                        goods.setStock(goods.getStock() + item.getQuantity());
                    }
                });
            }
        }
        return ResultModel.ok(goods);
    }
    
    public ResultModel addGoods(GoodsAddBody goodsAddBody, Admin admin){
        Goods goods = goodsMapper.getGoodsByName(goodsAddBody.getName(), goodsAddBody.getStoreroomId());
        if(goods != null){
            return ResultModel.error(ResultStatus.GOODS_NAME_EXIST);
        }
        Storeroom storeroom = storeroomMapper.selectByPrimaryKey(goodsAddBody.getStoreroomId());
        if(storeroom == null){
            return ResultModel.error(ResultStatus.GOODS_STOREROOM_NO_EXIST);
        }
        goods = new Goods();
        goods.setPrice(goodsAddBody.getPrice());
        goods.setName(goodsAddBody.getName());
        goods.setUnit(goodsAddBody.getUnit());
        goods.setStoreroomId(goodsAddBody.getStoreroomId());
        goods.setAdminId(admin.getId());
        goodsMapper.insertSelective(goods);
        
        return ResultModel.ok();
    }
    
    public ResultModel updateGoods(boolean isAdmin, GoodsAddBody goodsAddBody){
        Long goodsId = goodsAddBody.getId();
        boolean needUpdatePrice = false;
        if(goodsId == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
        if(goods == null){
            return ResultModel.error(ResultStatus.GOODS_NOT_EXIST);
        }
        goods.setName(goodsAddBody.getName());
        goods.setUnit(goodsAddBody.getUnit());
        if(isAdmin){
            if(goods.getPrice().compareTo(goodsAddBody.getPrice()) != 0){
                goods.setPrice(goodsAddBody.getPrice());
                needUpdatePrice = true;
            }
        }

        goodsMapper.updateByPrimaryKeySelective(goods);
        /**
         * 同步修改订单，的金额
         */
        if(needUpdatePrice) {
            List<OrderDetail> orderDetailList = orderMapper.getUnConfirmedDetailByGoodsId(goods.getId());
            for (OrderDetail orderDetail : orderDetailList) {
                orderDetail.setGoodsPrice(goodsAddBody.getPrice());
                orderMapper.updateUnConfirmedGoodsPrice(goodsAddBody.getPrice(), orderDetail.getId());
                orderMapper.reCalcAmount(orderDetail.getOrderId());
            }
        }
        return ResultModel.ok();
    }
    
    public ResultModel updateStock(Admin admin, Long goodsId, Long orderId, GoodsStockHistoryType type, 
                                    Integer stock, Integer frozenStock, String remark){
        Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
        if(goods == null){
            return ResultModel.error(ResultStatus.GOODS_NOT_EXIST);
        }
        if(goods.getStock() + stock < 0){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        if(goods.getFrozenStock() + frozenStock < 0){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        GoodsStockHistory history = new GoodsStockHistory();
        history.setGoodsId(goods.getId());
        history.setGoodsName(goods.getName());
        
        history.setRoomId(goods.getStoreroomId());
        Storeroom storeroom = storeroomMapper.selectByPrimaryKey(goods.getStoreroomId());
        if(storeroom!= null){
            history.setRoomName(storeroom.getName());
        }
        
        if(orderId != null && orderId.intValue() > 0){
            BaseOrder baseOrder = orderMapper.selectByPrimaryKey(orderId);
            if(baseOrder != null){
                Organ organ = organMapper.selectByPrimaryKey(baseOrder.getOrganId());
                if(organ != null){
                    history.setOrganId(organ.getId());
                    history.setOrganName(organ.getName());
                }
            }
        }
        
        history.setStock(stock);
        history.setRemaindingStock(goods.getStock() + stock);
        
        history.setFrozenStock(frozenStock);
        history.setRemaindingFrozenStock(goods.getFrozenStock() + frozenStock);
        
        history.setRemark(remark);
        history.setOrderId(orderId);
        history.setAdminId(admin.getId());
        history.setType(type.getType());

        goodsStockHistoryMapper.insertSelective(history);
        goods.setStock(goods.getStock() + stock);
        goods.setFrozenStock(goods.getFrozenStock() + frozenStock);
        goodsMapper.updateByPrimaryKeySelective(goods);
        return ResultModel.ok();
    }
    
    @Transactional
    @Synchronized
    public ResultModel updateStock(GoodsStockBody body, Admin admin){
        return updateStock(admin, body.getId(), body.getOrderId(), GoodsStockHistoryType.getInstance(body.getType()), 
                body.getStock(), 0, body.getRemark());
    }
    
    public ResultModel updateStockStatus(GoodsAddBody goodsAddBody){
        Long goodsId = goodsAddBody.getId();
        if(goodsId == null || goodsId.intValue() == 0){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
        if(goods == null){
            return ResultModel.error(ResultStatus.GOODS_NOT_EXIST);
        }
        
        goods.setStockStatus(goodsAddBody.getStockStatus());
        goodsMapper.updateByPrimaryKeySelective(goods);
        
        return ResultModel.ok();
    }
}
