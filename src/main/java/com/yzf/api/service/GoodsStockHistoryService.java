package com.yzf.api.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yzf.api.common.ResultModel;
import com.yzf.api.dao.GoodsStockHistoryMapper;
import com.yzf.api.pojo.GoodsStockHistoryDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsStockHistoryService {
    @Autowired
    private GoodsStockHistoryMapper goodsStockHistoryMapper;
    
    public ResultModel list(Long goodsId, Long organId, String startDate, String endDate, Integer type, Long storeroomId, int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<GoodsStockHistoryDetail> goodsStockHistoryDetailList = goodsStockHistoryMapper.getList(goodsId, organId, startDate, endDate, type, storeroomId);
        PageInfo<GoodsStockHistoryDetail> pageInfo = new PageInfo<>(goodsStockHistoryDetailList);
        return ResultModel.ok(pageInfo);
    }
}
