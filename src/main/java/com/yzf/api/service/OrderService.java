package com.yzf.api.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yzf.api.common.ResultModel;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.dao.GoodsMapper;
import com.yzf.api.dao.OrderMapper;
import com.yzf.api.exeption.BusinessException;
import com.yzf.api.model.*;
import com.yzf.api.req.body.GoodsStockBody;
import com.yzf.api.req.body.OrderAddBody;
import com.yzf.api.type.GoodsStockHistoryType;
import com.yzf.api.type.GoodsStockStatusType;
import com.yzf.api.type.OrderStatusType;
import com.yzf.api.utils.TextUtil;
import lombok.Synchronized;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;
    
    @Autowired
    private GoodsMapper goodsMapper;
    
    @Autowired
    private GoodsService goodsService;
    
    public ResultModel list(String orderNo, Integer orderState, Long organId, int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<BaseOrder> orderInfoList = orderMapper.getList(orderNo, orderState, organId);
        PageInfo<BaseOrder> pageInfo = new PageInfo<>(orderInfoList);
        return ResultModel.ok(pageInfo);
    }
    
    public ResultModel getDetail(String orderNo){
        BaseOrder orderInfo = orderMapper.getOrderByNo(orderNo);
        if(orderInfo == null){
            return ResultModel.error(ResultStatus.ORDER_NOT_EXIST);
        }
        List<OrderDetail> orderDetailList = orderMapper.getDetailList(orderInfo.getId());
        orderInfo.setGoodsList(orderDetailList);
        
        return ResultModel.ok(orderInfo);
    }
    
    @Transactional
    public ResultModel confirmOut(String orderNo, Admin admin){
        BaseOrder orderInfo = orderMapper.getOrderByNo(orderNo);
        if(orderInfo == null){
            return ResultModel.error(ResultStatus.ORDER_NOT_EXIST);
        }
        if(orderInfo.getOrderStatus() != OrderStatusType.UNCONFIRMED.getType()){
            return ResultModel.error(ResultStatus.ORDER_STATUS_ERROR);
        }
        
        orderInfo.setOrderStatus(OrderStatusType.OUT_CONFIRMED.getType());
        orderMapper.updateByPrimaryKeySelective(orderInfo);
        
        List<OrderDetail> orderDetailList = orderMapper.getDetailList(orderInfo.getId());
        for(OrderDetail orderDetail: orderDetailList){
            Goods goods = goodsMapper.selectByPrimaryKey(orderDetail.getGoodsId());
            orderDetail.setGoodsPrice(goods.getPrice());
            orderMapper.updateUnConfirmedGoodsPrice(goods.getPrice(), orderDetail.getId());
            goodsService.updateStock(admin, orderDetail.getGoodsId(), orderDetail.getOrderId(), 
                    GoodsStockHistoryType.ORDER, 0, orderDetail.getQuantity()*-1, 
                    GoodsStockHistoryType.ORDER.getDesc());
        }
        
        return ResultModel.ok();
    }
    
    @Transactional
    public ResultModel cancel(String orderNo, Admin admin){
        BaseOrder orderInfo = orderMapper.getOrderByNo(orderNo);
        if(orderInfo == null){
            return ResultModel.error(ResultStatus.ORDER_NOT_EXIST);
        }
        if(orderInfo.getOrderStatus() != OrderStatusType.UNCONFIRMED.getType()){
            return ResultModel.error(ResultStatus.ORDER_STATUS_ERROR);
        }
        
        orderInfo.setOrderStatus(OrderStatusType.CANCELED.getType());
        orderMapper.updateByPrimaryKeySelective(orderInfo);
        
        //退还库存
        List<OrderDetail> orderDetailList = orderMapper.getDetailList(orderInfo.getId());
        for(OrderDetail orderDetail: orderDetailList){
            // 取消订单解冻
            goodsService.updateStock(admin, orderDetail.getGoodsId(), orderDetail.getOrderId(), 
                    GoodsStockHistoryType.UNFROZEN, orderDetail.getQuantity(), orderDetail.getQuantity()*-1, 
                    GoodsStockHistoryType.UNFROZEN.getDesc());
        }
        return ResultModel.ok();
    }
    
    public ResultModel confirmIn(String orderNo){
        BaseOrder orderInfo = orderMapper.getOrderByNo(orderNo);
        if(orderInfo == null){
            return ResultModel.error(ResultStatus.ORDER_NOT_EXIST);
        }
        if(orderInfo.getOrderStatus() != OrderStatusType.OUT_CONFIRMED.getType()){
            return ResultModel.error(ResultStatus.ORDER_STATUS_ERROR);
        }
        
        orderInfo.setOrderStatus(OrderStatusType.IN_CONFIRMED.getType());
        orderMapper.updateByPrimaryKeySelective(orderInfo);

        return ResultModel.ok();
    }
    
    @Synchronized
    @Transactional
    public ResultModel update(OrderAddBody orderAddBody, Admin admin){
        String orderNo = orderAddBody.getOrderNo();
        if(StringUtils.isBlank(orderNo)){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        BaseOrder order = orderMapper.getOrderByNo(orderNo);
        if(order == null){
            return ResultModel.error(ResultStatus.ORDER_NOT_EXIST);
        }
        
        List<OrderDetail> detailList = orderMapper.getDetailList(order.getId());
        for(OrderDetail orderDetail: detailList){
            goodsService.updateStock(admin, orderDetail.getGoodsId(), order.getId(),
                    GoodsStockHistoryType.ORDER_MODIFY, orderDetail.getQuantity(), 
                    orderDetail.getQuantity() * -1, GoodsStockHistoryType.ORDER_MODIFY.getDesc());
        }
        orderMapper.clearDetail(order.getId());
        
        List<GoodsStockBody> goodsStockBodyList = orderAddBody.getGoodsList();
        if(goodsStockBodyList == null || goodsStockBodyList.isEmpty()){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        BigDecimal amount = new BigDecimal(0);
        List<OrderDetail> orderDetailList = new ArrayList<>();
        for(GoodsStockBody body: goodsStockBodyList){
            Goods goods = goodsMapper.selectByPrimaryKey(body.getId());
            if(goods.getStock() < body.getStock()){
                throw new BusinessException(ResultStatus.GOODS_STOCK_NOT_ENOUGH);
            }

            goodsService.updateStock(admin, body.getId(), order.getId(), GoodsStockHistoryType.FROZEN, 
                    body.getStock() * -1, body.getStock(), GoodsStockHistoryType.FROZEN.getDesc());

            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(order.getId());
            orderDetail.setGoodsId(goods.getId());
            orderDetail.setGoodsPrice(goods.getPrice());
            orderDetail.setQuantity(body.getStock());
            orderDetailList.add(orderDetail);

            amount = amount.add(goods.getPrice().multiply(new BigDecimal(body.getStock())));
        }

        order.setAmount(amount);
        orderMapper.updateByPrimaryKeySelective(order);
        orderMapper.insertDetailBatch(orderDetailList);
        return ResultModel.ok();
    }
    
    @Transactional(rollbackFor = Exception.class)
    @Synchronized
    public ResultModel add(OrderAddBody orderAddBody, Admin admin){
        List<GoodsStockBody> goodsStockBodyList = orderAddBody.getGoodsList();
        if(goodsStockBodyList == null || goodsStockBodyList.isEmpty()){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        
        BigDecimal amount = new BigDecimal(0);
        BaseOrder order = new BaseOrder();
        order.setOrderNo(TextUtil.genOrderNo(orderAddBody.getOrganId(), admin.getId()));
        order.setStoreroomId(orderAddBody.getStoreroomId());
        order.setOrganId(orderAddBody.getOrganId());
        order.setOriginInfo("");
        order.setDelivery(orderAddBody.getDelivery());
        orderMapper.insertSelective(order);
        
        List<OrderDetail> orderDetailList = new ArrayList<>();
        
        for(GoodsStockBody body: goodsStockBodyList){
            Goods goods = goodsMapper.selectByPrimaryKey(body.getId());
            if(goods.getStockStatus() == GoodsStockStatusType.STOP.getType()){
                throw new BusinessException(ResultStatus.GOODS_STOCK_STOP);
            }
            if(goods.getStock() < body.getStock()){
                throw new BusinessException(ResultStatus.GOODS_STOCK_NOT_ENOUGH);
            }
            goodsService.updateStock(admin, body.getId(), order.getId(), 
                    GoodsStockHistoryType.FROZEN, body.getStock() * -1, body.getStock(), 
                    GoodsStockHistoryType.FROZEN.getDesc());
            
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(order.getId());
            orderDetail.setGoodsId(goods.getId());
            orderDetail.setGoodsPrice(goods.getPrice());
            orderDetail.setQuantity(body.getStock());
            orderDetail.setUnit(goods.getUnit());
            orderDetailList.add(orderDetail);
            
            amount = amount.add(goods.getPrice().multiply(new BigDecimal(body.getStock())));
        }
        
        order.setAmount(amount);
        orderMapper.updateByPrimaryKeySelective(order);
        orderMapper.insertDetailBatch(orderDetailList);

        ResultModel rt = this.getDetail(order.getOrderNo());
        order.setOriginInfo(JSON.toJSONString(rt.getData()));
        orderMapper.updateByPrimaryKeySelective(order);
        
        return ResultModel.ok();
    }
    
}
