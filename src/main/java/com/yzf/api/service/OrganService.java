package com.yzf.api.service;

import com.yzf.api.common.ResultModel;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.dao.*;
import com.yzf.api.model.*;
import com.yzf.api.pojo.OrganAdminDetail;
import com.yzf.api.req.body.OrganAddBody;
import com.yzf.api.req.body.OrganAdminsUpdateBody;
import com.yzf.api.type.RoleType;
import com.yzf.api.utils.RoleUtil;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrganService {
    
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private OrganMapper organMapper;
    @Autowired
    private AdminOrganMapper adminOrganMapper;
    @Autowired
    private OrganStoreroomMapper organStoreroomMapper;
    @Autowired
    private StoreroomMapper storeroomMapper;
    
    public ResultModel listAll(){
        List<Organ> organList = organMapper.selectAll();
        for(Organ organ: organList){
            List<Admin> managers = adminMapper.getOrganAdminsByRoleId(organ.getId(), RoleUtil.getRoleIdByRoleType(RoleType.ORGAN_MANAGER));
            if(managers != null && managers.size() > 0){
                organ.setAdmin(managers.get(0));
            }
            List<Admin> staffs = adminMapper.getOrganAdminsByRoleId(organ.getId(), RoleUtil.getRoleIdByRoleType(RoleType.ORGAN_STAFF));
            organ.setStaffs(staffs);
            
            Storeroom storeroom = storeroomMapper.getStoreroomByOrganId(organ.getId());
            organ.setStoreroom(storeroom);
        }
        return ResultModel.ok(organList);
    }
    
    @Transactional
    @Synchronized
    public ResultModel add(Admin admin, OrganAddBody body){
        if(organMapper.getByName(body.getName()) != null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        Organ organ = new Organ();
        organ.setName(body.getName());
        organ.setCreateBy(admin.getId());
        organMapper.insertSelective(organ);
        
        AdminOrgan adminOrgan = new AdminOrgan();
        adminOrgan.setAdminId(body.getAdminId());
        adminOrgan.setOrganId(organ.getId());
        adminOrganMapper.insert(adminOrgan);

        OrganStoreroom organStoreroom = new OrganStoreroom();
        organStoreroom.setOrganId(organ.getId());
        organStoreroom.setStoreroomId(body.getRoomId());
        organStoreroomMapper.insert(organStoreroom);
        
        return ResultModel.ok();
    }
    
    @Transactional
    @Synchronized
    public ResultModel update(Admin admin, OrganAddBody body){
        Long adminId = body.getAdminId();
        if(body.getId() == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Organ organ = organMapper.selectByPrimaryKey(body.getId());
        if(organ == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        if(!organ.getName().equals(body.getName())){
            if(organMapper.getByName(body.getName()) != null){
                return ResultModel.error(ResultStatus.PARAMETER_ERROR);                                                                                             
            }
        }
        
        organ.setName(body.getName());
        organMapper.updateByPrimaryKeySelective(organ);
        
        List<AdminOrgan> adminOrgans = adminOrganMapper.getByRoleId(organ.getId(), Long.valueOf(RoleType.ORGAN_MANAGER.getType()));
        if(adminId == null || adminId.intValue() == 0) { // 删除店长
            if(adminOrgans != null && adminOrgans.size() > 0){
                adminOrgans.forEach(item->adminOrganMapper.deleteByPrimaryKey(item.getId()));
            }
        }else{
            if(adminOrgans != null && adminOrgans.size() > 0){ // 店长已经存在，更新
                AdminOrgan ao = adminOrgans.get(0); // 理论上只有条记录
                ao.setAdminId(body.getAdminId());
                adminOrganMapper.updateByPrimaryKeySelective(ao);
            }else{
                AdminOrgan ao = new AdminOrgan();
                ao.setOrganId(organ.getId());
                ao.setAdminId(body.getAdminId());
                adminOrganMapper.insert(ao);
            }
        }
        
        OrganStoreroom organStoreroom = organStoreroomMapper.getByOrganId(organ.getId());
        if(organStoreroom != null){
            organStoreroom.setStoreroomId(body.getRoomId());
            organStoreroomMapper.updateByPrimaryKeySelective(organStoreroom);
        }else{
            OrganStoreroom os = new OrganStoreroom();
            os.setStoreroomId(body.getRoomId());
            os.setOrganId(organ.getId());
            organStoreroomMapper.insert(os);
        }
        
        return ResultModel.ok();
    }
    
    public ResultModel updateStatus(Long id, int status){
        Organ organ = organMapper.selectByPrimaryKey(id);
        if(organ == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        organ.setStatus(status);
        organMapper.updateByPrimaryKeySelective(organ);
        
        return ResultModel.ok();
    }
    
    public ResultModel getAdmins(Long id){
        if(id == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Organ organ = organMapper.selectByPrimaryKey(id);
        if(organ == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        OrganAdminDetail organAdminDetail = new OrganAdminDetail();
        
        List<Admin> keepers = adminMapper.getOrganAdminsByRoleId(id, Long.valueOf(RoleType.ORGAN_MANAGER.getType()));
        List<Admin> staffs = adminMapper.getOrganAdminsByRoleId(id, Long.valueOf(RoleType.ORGAN_STAFF.getType()));
        
        if(keepers != null && keepers.size() > 0){
            organAdminDetail.setKeeper(keepers.get(0));
        }
        organAdminDetail.setStaffs(staffs);
        
        return ResultModel.ok(organAdminDetail);
    }
    
    @Transactional
    @Synchronized
    public ResultModel updateAdmins(OrganAdminsUpdateBody body){
        if(body.getOrganId() == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Organ organ = organMapper.selectByPrimaryKey(body.getOrganId());
        if(organ == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        
        adminOrganMapper.deleteByOrganId(organ.getId());
        if(body.getKeeperId() != null && body.getKeeperId() > 0){
            AdminOrgan ao = new AdminOrgan();
            ao.setAdminId(body.getKeeperId());
            ao.setOrganId(organ.getId());
            adminOrganMapper.insert(ao);
        }
        
        body.getStaffs().forEach(item->{
            AdminOrgan ao = new AdminOrgan();
            ao.setAdminId(item);
            ao.setOrganId(organ.getId());
            adminOrganMapper.insert(ao);
        });
        
        return ResultModel.ok();
    }
}
