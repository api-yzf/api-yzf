package com.yzf.api.service;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class RedisService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public long incr(final String key) {
        Long val =
                this.stringRedisTemplate.execute((RedisCallback<Long>) connection -> connection.incr(key.getBytes()));
        return val == null ? 0L : val;
    }

    public Long incr(final String key, final Long liveTime) {
        return stringRedisTemplate.execute((RedisCallback<Long>) connection -> {
            long result = connection.incr(key.getBytes());
            if (liveTime != null) {
                connection.expire(key.getBytes(), liveTime);
            }
            return result;
        });
    }


    public void expire(final String key, int minutes) {
        this.stringRedisTemplate.expire(key, minutes, TimeUnit.MINUTES);
    }

    public Long del(String... keys) {
        return (Long) this.stringRedisTemplate.execute((RedisCallback<Object>) connection -> {
            long result = 0;
            for (int i = 0; i < keys.length; i++) {
                result = connection.del(keys[i].getBytes());
            }
            return result;
        });
    }
    public void addSet(String key, String value) {
        this.stringRedisTemplate.opsForSet().add(key, value);
    }

    public void delSet(String key, String value) {
        this.stringRedisTemplate.opsForSet().remove(key ,value);
    }

    public Set<String> listSetMembers(String key) {
        return this.stringRedisTemplate.opsForSet().members(key);
    }

    public String get(String key) {
        return this.stringRedisTemplate.opsForValue().get(key);
    }

    public void set(String key, String val) {
        this.stringRedisTemplate.opsForValue().set(key, val);
    }

    public void set(String key, String val, int minutes) {
        this.stringRedisTemplate.opsForValue().set(key, val, minutes, TimeUnit.MINUTES);
    }

    public long incrBy(final String key, long stepSize) {
        return stringRedisTemplate.opsForValue().increment(key, stepSize);
    }
}
