package com.yzf.api.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yzf.api.common.ResultModel;
import com.yzf.api.dao.OrderMapper;
import com.yzf.api.pojo.OrderStat;
import com.yzf.api.pojo.OrderStatDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class StateService {
    
    @Autowired
    private OrderMapper orderMapper;
    
    public ResultModel getOrderStat(Long organId, Long roomId, String startDate, String endDate){
        OrderStat orderStat = orderMapper.getStatSum(organId, roomId, startDate, endDate);
        if(orderStat == null){
            orderStat = new OrderStat();
            orderStat.setAmount(BigDecimal.ZERO);
            orderStat.setQuantity(0L);
        }
        return ResultModel.ok(orderStat);
    }
    
    public ResultModel getOrganOrderList(Long organId, Integer type, String startDate, String endDate, int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<OrderStatDetail> list;
        if(type == 1){ // 按天
            list = orderMapper.getStatListByDay(organId, null, startDate, endDate);
        }else{ // 按月
            list = orderMapper.getStatListByMonth(organId, null, startDate, endDate);
        }

        PageInfo<OrderStatDetail> pageInfo = new PageInfo<>(list);
        return ResultModel.ok(pageInfo);
    }
    
    public ResultModel getRoomOrderList(Long roomId, Integer type, String startDate, String endDate, int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<OrderStatDetail> list;
        if(type == 1){ // 按天
            list = orderMapper.getStatListByDay(null, roomId, startDate, endDate);
        }else{ // 按月
            list = orderMapper.getStatListByMonth(null, roomId, startDate, endDate);
        }

        PageInfo<OrderStatDetail> pageInfo = new PageInfo<>(list);
        return ResultModel.ok(pageInfo);
    }
}
