package com.yzf.api.service;

import com.google.common.collect.HashBasedTable;
import com.yzf.api.common.ResultModel;
import com.yzf.api.common.ResultStatus;
import com.yzf.api.dao.AdminMapper;
import com.yzf.api.dao.AdminStoreroomMapper;
import com.yzf.api.dao.StoreroomMapper;
import com.yzf.api.model.*;
import com.yzf.api.pojo.OrganAdminDetail;
import com.yzf.api.req.body.StoreroomAddBody;
import com.yzf.api.req.body.StoreroomAdminsUpdateBody;
import com.yzf.api.type.RoleType;
import com.yzf.api.utils.RoleUtil;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class StoreroomService {
    
    @Autowired
    private AdminMapper adminMapper;
    
    @Autowired
    private StoreroomMapper storeroomMapper;
    
    @Autowired
    private AdminStoreroomMapper adminStoreroomMapper;
    
    public ResultModel listAll(){
        List<Storeroom> storeroomList = storeroomMapper.selectAll();

        for (Storeroom storeroom : storeroomList) {
            Long id = storeroom.getId();
            List<Admin> keepers = adminMapper.getStoreroomAdminsByRoleId(id, Long.valueOf(RoleType.ROOM_MANAGER.getType()));
            List<Admin> staffs = adminMapper.getStoreroomAdminsByRoleId(id, Long.valueOf(RoleType.ROOM_STAFF.getType()));
            if(keepers != null && keepers.size() > 0){
                storeroom.setAdmin(keepers.get(0));
            }
            storeroom.setStaffs(staffs);
        }
        
        return ResultModel.ok(storeroomList);
    }
    
    @Transactional
    @Synchronized
    public ResultModel add(Admin admin, StoreroomAddBody body){
        if(storeroomMapper.getByName(body.getName()) != null){
            return ResultModel.error(ResultStatus.ROOM_NAME_EXIST);
        }
        Storeroom room = new Storeroom();
        room.setName(body.getName());
        room.setCreateBy(admin.getId());
        storeroomMapper.insertSelective(room);

        AdminStoreroom adminStoreroom = new AdminStoreroom();
        adminStoreroom.setAdminId(body.getAdminId());
        adminStoreroom.setStoreroomId(room.getId());
        adminStoreroomMapper.insertSelective(adminStoreroom);
        
        return ResultModel.ok();
    }
    
    @Transactional
    @Synchronized
    public ResultModel update(Admin admin, StoreroomAddBody body){
        Long adminId = body.getAdminId();
        if(body.getId() == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Storeroom storeroom = storeroomMapper.selectByPrimaryKey(body.getId());
        if(storeroom == null){
            return ResultModel.error(ResultStatus.GOODS_STOREROOM_NO_EXIST);
        }
        if(!storeroom.getName().equals(body.getName())){
            if(storeroomMapper.getByName(body.getName()) != null){
                return ResultModel.error(ResultStatus.ROOM_NAME_EXIST);
            }
            storeroom.setName(body.getName());
            storeroomMapper.updateByPrimaryKeySelective(storeroom);
        }
        
        List<AdminStoreroom> adminStorerooms = adminStoreroomMapper.getByRoleId(storeroom.getId(), Long.valueOf(RoleType.ROOM_MANAGER.getType()));
        if(adminId == null || adminId.intValue() == 0){
            if(adminStorerooms!=null && adminStorerooms.size() > 0){ // 删除
                adminStorerooms.forEach(item->adminStoreroomMapper.deleteByPrimaryKey(item.getId()));
            }
        }else{
            if(adminStorerooms != null && adminStorerooms.size() > 0){ // 店长已经存在，更新
                AdminStoreroom as = adminStorerooms.get(0); // 理论上只有条记录
                as.setAdminId(body.getAdminId());
                adminStoreroomMapper.updateByPrimaryKeySelective(as);
            }else{
                AdminStoreroom as = new AdminStoreroom();
                as.setStoreroomId(storeroom.getId());
                as.setAdminId(body.getAdminId());
                adminStoreroomMapper.insertSelective(as);
            }
        }
        
        return ResultModel.ok();
    }
    
    public ResultModel updateStatus(Long id, int status){
        if(id == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Storeroom room = storeroomMapper.selectByPrimaryKey(id);
        if(room == null){
            return ResultModel.error(ResultStatus.GOODS_STOREROOM_NO_EXIST);
        }
        
        room.setStatus(status);
        storeroomMapper.updateByPrimaryKeySelective(room);
        
        return ResultModel.ok();
    }
    
    public ResultModel getAdmins(Long id){
        if(id == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Storeroom storeroom = storeroomMapper.selectByPrimaryKey(id);
        if(storeroom == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }

        OrganAdminDetail organAdminDetail = new OrganAdminDetail();

        List<Admin> keepers = adminMapper.getStoreroomAdminsByRoleId(id, Long.valueOf(RoleType.ROOM_MANAGER.getType()));
        List<Admin> staffs = adminMapper.getStoreroomAdminsByRoleId(id, Long.valueOf(RoleType.ROOM_STAFF.getType()));

        if(keepers != null && keepers.size() > 0){
            organAdminDetail.setKeeper(keepers.get(0));
        }
        organAdminDetail.setStaffs(staffs);

        return ResultModel.ok(organAdminDetail);
    }

    @Transactional
    @Synchronized
    public ResultModel updateAdmins(StoreroomAdminsUpdateBody body){
        if(body.getRoomId() == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }
        Storeroom room = storeroomMapper.selectByPrimaryKey(body.getRoomId());
        if(room == null){
            return ResultModel.error(ResultStatus.PARAMETER_ERROR);
        }

        adminStoreroomMapper.deleteByRoomId(room.getId());
        
        if(body.getKeeperId() != null && body.getKeeperId() > 0){
            AdminStoreroom as = new AdminStoreroom();
            as.setAdminId(body.getKeeperId());
            as.setStoreroomId(room.getId());
            adminStoreroomMapper.insertSelective(as);
        }

        body.getStaffs().forEach(item->{
            AdminStoreroom as = new AdminStoreroom();
            as.setAdminId(item);
            as.setStoreroomId(room.getId());
            adminStoreroomMapper.insertSelective(as);
        });

        return ResultModel.ok();
    }
}
