package com.yzf.api.type;

public enum AdminStatusType {
    NORMAL(1, "正常"),
    FORBIDDEN(0, "暂停"),
    DELETE(-1, "删除");

    private final int type;

    private final String desc;

    AdminStatusType(int type, String desc){
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return this.type;
    }

    public String getDesc() {
        return this.desc;
    }
}
