package com.yzf.api.type;

public enum GoodsStockHistoryType {
    OUT(1, "入库"),
    MODIFY(2, "后台修改"),
    ORDER(3, "出库"),
    FROZEN(4, "下单冻结"),
    UNFROZEN(5, "取消解冻"),
    ORDER_MODIFY(6, "修改订单");

    private final int type;

    private final String desc;

    GoodsStockHistoryType(int type, String desc){
        this.type = type;
        this.desc = desc;
    }

    public static GoodsStockHistoryType getInstance(int type) {
        for(GoodsStockHistoryType goodsStockHistoryType : GoodsStockHistoryType.values()) {
            if(goodsStockHistoryType.getType() == type) {
                return goodsStockHistoryType;
            }
        }
        return null;
    }

    public String getDesc() {
        return this.desc;
    }

    public int getType() {
        return this.type;
    }
}
