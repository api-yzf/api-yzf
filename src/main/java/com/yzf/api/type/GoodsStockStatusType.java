package com.yzf.api.type;

public enum GoodsStockStatusType {
    NORMAL(1, "正常"),
    STOP(0, "暂停出库");

    private final int type;

    private final String desc;

    GoodsStockStatusType(int type, String desc){
        this.type = type;
        this.desc = desc;
    }

    public static GoodsStockStatusType getInstance(int type) {
        for(GoodsStockStatusType goodsStockStatusType : GoodsStockStatusType.values()) {
            if(goodsStockStatusType.getType() == type) {
                return goodsStockStatusType;
            }
        }
        return null;
    }

    public String getDesc() {
        return this.desc;
    }

    public int getType() {
        return this.type;
    }
}
