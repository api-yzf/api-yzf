package com.yzf.api.type;

public enum OrderStatusType {
    UNCONFIRMED(1, "待发货"),
    OUT_CONFIRMED(2, "已发货（出库）"),
    IN_CONFIRMED(3, "已完成（确认收货）"),
    CANCELED(4, "已取消");
    
    private final int type;

    private final String desc;

    OrderStatusType(int type, String desc){
        this.type = type;
        this.desc = desc;
    }

    public static OrderStatusType getInstance(int type) {
        for(OrderStatusType orderStatusType : OrderStatusType.values()) {
            if(orderStatusType.getType() == type) {
                return orderStatusType;
            }
        }
        return null;
    }

    public String getDesc() {
        return this.desc;
    }

    public int getType() {
        return this.type;
    }
}
