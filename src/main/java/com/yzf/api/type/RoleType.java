package com.yzf.api.type;

public enum RoleType {
    ALL(100, "All"),
    ROOM_STAFF(1, "ROOM_STAFF"), // 库管
    ORGAN_MANAGER(2, "ORGAN_MANAGER"), // 店长
    ADMIN(3,"ADMIN"), //管理员
    BASE_ORGAN_MANAGER(4, "BASE_ORGAN_MANAGER"), // 总部管理员
    ORGAN_STAFF(5, "ORGAN_STAFF"), // 店员
    ROOM_MANAGER(6, "ROOM_MANAGER"); // 仓库负责任

    private final int type;

    private final String desc;

    RoleType(int type, String desc){
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return this.type;
    }

    public String getDesc() {
        return this.desc;
    }

}
