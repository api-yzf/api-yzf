package com.yzf.api.utils;

import com.yzf.api.model.Role;
import com.yzf.api.type.RoleType;

public class RoleUtil {
    
    public static boolean isAdmin(Role role){
        if(role == null){
            return false;
        }
        String name = role.getName();
        if(name == null){
            return false;
        }
        return name.toLowerCase().equals(RoleType.ADMIN.getDesc().toLowerCase());
    }
    
    public static boolean isBaseOrganManager(Role role){
        if(role == null){
            return false;
        }
        String name = role.getName();
        if(name == null){
            return false;
        }
        return name.toLowerCase().equals(RoleType.BASE_ORGAN_MANAGER.getDesc().toLowerCase());
    }
    
    public static boolean isOrganMember(Role role){
        if(role == null){
            return false;
        }
        String name = role.getName();
        if(name == null){
            return false;
        }
        String nm = name.toLowerCase();
        return nm.equals(RoleType.ORGAN_MANAGER.getDesc().toLowerCase()) || nm.equals(RoleType.ORGAN_STAFF.getDesc().toLowerCase());
    }
    
    public static boolean isStoreroomMember(Role role){
        if(role == null){
            return false;
        }
        String name = role.getName();
        if(name == null){
            return false;
        }
        String nm = name.toLowerCase();
        return nm.equals(RoleType.ROOM_MANAGER.getDesc().toLowerCase()) || nm.equals(RoleType.ROOM_STAFF.getDesc().toLowerCase());
    }
    
    public static Long getRoleIdByRoleType(RoleType roleType){
        return Long.valueOf(roleType.getType());
    }
}
