package com.yzf.api.utils;

import org.apache.commons.lang3.StringUtils;
import java.util.*;

/**
 * @author
 * @author: quhc
 * @date: 2019/9/25 13:38
 */

public class TextUtil{

    private static final String base = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKMNPQRSTUVWXYZ23456789";

    private static final String intBase = "0123456789";
    
    public static String genOrderNo(Long organId, Long adminId){
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {//有可能是负数
            hashCodeV = -hashCodeV;
        }
        return String.format("N%s%s%s", String.format("%04d", organId), String.format("%04d", adminId), String.format("%010d", hashCodeV));
    }

    /**
     * 使用java正则表达式去掉多余的.与0
     * @param s
     * @return
     */
    public static String subZeroAndDot(String s) {
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");// 去掉多余的0
            s = s.replaceAll("[.]$", "");// 如最后一位是.则去掉
        }
        return s;
    }

    public static String genRndStr(int length, boolean isInt) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            if (isInt) {
                int number = random.nextInt(intBase.length());
                sb.append(intBase.charAt(number));
            } else {
                int number = random.nextInt(base.length());
                sb.append(base.charAt(number));
            }
        }
        return sb.toString();
    }

    public static long parseLong(String v) {
        if(StringUtils.isBlank(v)) {
            return 0L;
        }
        try{
            return Long.parseLong(v);
        }catch(NumberFormatException e) {
            return 0L;
        }
    }

    public static double parseDouble(String v) {
        if(StringUtils.isBlank(v)) {
            return 0L;
        }
        try{
            return Double.parseDouble(v);
        }catch(NumberFormatException e) {
            return 0L;
        }
    }

    public static int parseInt(String v) {
        try{
            return Integer.parseInt(v);
        }catch(NumberFormatException e) {
            return 0;
        }
    }

    public static boolean isUserId(String name) {
        if(parseInt(name)==0) {
            return false;
        }
        return true;
    }

    public static String maskMobile(String mobile) {
        if(StringUtils.isBlank(mobile) || mobile.length() < 11) {
            return "";
        }
        return String.format("%s****%s", mobile.substring(0, 3), mobile.substring(7, 11));
    }
    
}
