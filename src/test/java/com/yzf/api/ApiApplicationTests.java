package com.yzf.api;

import com.alibaba.fastjson.JSON;
import com.yzf.api.dao.AdminMapper;
import com.yzf.api.dao.GoodsMapper;
import com.yzf.api.dao.OrganMapper;
import com.yzf.api.dao.RoleMapper;
import com.yzf.api.model.Goods;
import com.yzf.api.service.GoodsService;
import com.yzf.api.service.OrderService;
import com.yzf.api.service.OrganService;
import com.yzf.api.service.StoreroomService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import tk.mybatis.spring.annotation.MapperScan;

import java.math.BigDecimal;

@SpringBootTest
@EnableCaching
class ApiApplicationTests {
    
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private GoodsService goodsService;
    
    @Test
    void contextLoads() {
//        System.out.println(JSON.toJSONString(adminMapper.getAdminByMobile("13812340001")));
//        System.out.println(JSON.toJSONString(roleMapper.getRolesByAdminId(1L)));
//        System.out.println(JSON.toJSONString(goodsMapper.selectByPrimaryKey()));
//        Goods goods=new Goods();
//        goods.setName("羊肉");
//        goods.setUnit("斤");
//        goods.setPrice(new BigDecimal(88.9));
//        
//        goodsMapper.insertSelective(goods);

//        System.out.println(JSON.toJSONString(goodsService.detail(1L)));
        System.out.println(roleMapper.selectAll());
        System.out.println(roleMapper.selectByPrimaryKey(3L));
    }
    
    @Test
    void goodsTest(){
//        System.out.println(JSON.toJSONString(goodsService.listGoods(Long.valueOf(1), "", 1, 10)));
    }
    
    @Autowired
    private OrganService organService;
    
    @Autowired
    private OrganMapper organMapper;
    
    @Test
    void organTest(){
//        System.out.println(JSON.toJSONString(organService.listAll()));
        System.out.println(JSON.toJSONString(organMapper.getOrganByAdminId(2L)));
    }
    
    @Autowired
    private StoreroomService storeroomService;
    @Test
    void storeroomTest(){
        System.out.println(JSON.toJSONString(storeroomService.listAll()));
    }

    @Autowired
    private OrderService orderService;
    @Test
    void orderTest(){
        System.out.println(JSON.toJSONString(orderService.list("", 0, 1L, 1, 10)));
    }
}
